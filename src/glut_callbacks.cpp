#include "glut_callbacks.hpp"

// OpenGL and GLUT initialization
void initGL() {

	// Setting window's initial position
	glutInitWindowPosition(900, 0);
	// Setting initial window's size: initialized with the size of the video
	glutInitWindowSize(widthFrame, heightFrame);

	// Setting up display mode
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	// Creating window and setting its name
	ogl_display = glutCreateWindow("GhostBusters AR");

	glutDisplayFunc(display);
	glutIdleFunc(idle);
	//glutReshapeFunc(resize); // Not implemented yet
	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);

	// Setting up clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// and depth clear value
	glClearDepth(1.0);

	//glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

// Keyboard function
void keyboard(unsigned char key, int x, int y) {
	switch (key) {

	// Exit program
	case KEY_ESCAPE:
		exit(0);
		break;

	// Toggle "ghost" render mode
	case 'g':
	case 'G':
		ghost = !ghost;
		glutPostRedisplay();
		break;

	// Toggle scene debug display
	case 's':
	case 'S':
		glScene = !glScene;
		if (glScene)
			printSceneHelp();
		break;

	default:
		break;
	}

}

// Mouse events
void mouse(int b, int s, int x, int y) {
	// nothing for now
}

// Display function
void display () {
	// Clearing color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// Showing Camera Capture in OpenCV window [debug]
	if (debugMode) {
		imshow(ocv_debug, currentFrame);
		waitKey(10); // add a delay so that the window has time to update
	}

	// Calling ArUco draw function
	arucoManager->drawScene();

	// Swapping GLUT buffers
	glutSwapBuffers();
}

// Idle function
void idle () {

	// Getting current frame
	cap >> currentFrame;

	// Calling ArUco idle
	arucoManager->idle(currentFrame);

	// Redisplay GLUT window
	glutPostRedisplay();
}


void printSceneHelp () {
	cout << "Marker id -> color\n"
		<< "0 -> Cyan\n"
		<< "1 -> Gray\n"
		<< "2 -> Pink\n"
		<< flush;
}