#ifndef GB_GLUT
#define GB_GLUT

#include "include.hpp"

#include "ArucoGL.hpp"

const unsigned int KEY_ESCAPE = 27;

// Extern definitions

	extern int widthFrame;
	extern int heightFrame;

	extern GLint ogl_display;

	extern VideoCapture cap;
	extern Mat currentFrame;
	extern bool debugMode;
	extern string ocv_debug;

	extern ArucoGL* arucoManager;
	extern bool ghost;

// Initialization of OpenGL
void initGL();

// Keyboard function
void keyboard(unsigned char key, int x, int y);

// Mouse events
void mouse(int b, int s, int x, int y);

// Display function
void display();

// Idle function
void idle();


/* Helpers */
void printSceneHelp ();

#endif //GB_GLUT