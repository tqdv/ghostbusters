#include "main.hpp"

// Camera initialization
void initCam (int &argc, char ** &argv) {
	bool camid_ok = false;

	if (argc >= 2) {
		try {
			cameraID = stoi(argv[1]);
			camid_ok = true;
		} catch (std::exception e) {}
	}
	if ( ! camid_ok ) {
		cout << "Entrez l'identifiant de la camera : " << flush;
		cin >> cameraID;
	}

	cap.open(cameraID);
	if (!cap.isOpened()) {
		cerr << "Erreur lors de l'initialisation de la capture de la camera !" << endl;
		cerr << "Fermeture..." << endl;
		exit(EXIT_FAILURE);
	}
	else {
		// first frame
		cap >> currentFrame;
	}

	// Getting width/height of the images
	widthFrame = cap.get(CAP_PROP_FRAME_WIDTH);
	heightFrame = cap.get(CAP_PROP_FRAME_HEIGHT);
	cout << "Frame width  = " << widthFrame << endl;
	cout << "Frame height = " << heightFrame << endl;

	// OpenCV window
	ocv_debug = "OpenCV camera stream [debug]";
	namedWindow(ocv_debug, WINDOW_AUTOSIZE);
}

// Exit function
void exitCleaner() {

	// Destroy OpenCV debug window
	destroyWindow(ocv_debug);

	// Release capture
	cap.release();

	// Deleting ArUco manager
	if (arucoManager) {
		delete(arucoManager);
		arucoManager = NULL;
	}

	// Deleting GLUT window
	if (ogl_display) {
		glutDestroyWindow(ogl_display);
	}

}

int main (int argc, char** argv) {
	// init glut here as it complains otherwise
	glutInit(&argc, argv);

	// States initialization
	debugMode = true;
	ghost = true;

	// Print a welcome message, and the OpenCV version
	cout << "Using OpenCV v" << CV_VERSION << endl;
	cout << "Welcome to GhostBusters AR" << endl;
	cout << "Hot keys :" << endl
		<< "\tESC - quit the program" << endl
		<< "\tG   - toggle ghost render mode" << endl
		<< "\tS   - toggle world view mode" << endl;

	initCam(argc, argv);

	// Creating the ArUco object
	arucoManager = new ArucoGL("camera.yml", 0.035f,"mansion.yml", widthFrame, heightFrame);
	cout << "ArUco OK" << endl << endl;
	
	// Exit function
	atexit(exitCleaner);

	// OpenGL/GLUT Initialization
	initGL();

	// Starting GLUT main loop
	glutMainLoop();

	return 0;
}
