#ifndef GB_ARUCOGL
#define GB_ARUCOGL

#include "include.hpp"

extern bool ghost;
extern bool debugMode;
extern bool glScene;
extern Mat currentFrame;

// Manage Aruco in OpenGL/GLUT
class ArucoGL {
	// Attributes
protected:
	// Intrinsics file for the camera
	string intrinsicFile;
	// Camera parameters
	CameraParameters  cameraParams;

	// Size of the markers (in meters)
	float markerSize;

	// The Marker Detector
	MarkerDetector    mDetector;
	// Vector of detected markers in the image
	vector<Marker>    markersList;

	// Marker map parameters
	MarkerMap mmap;
	// MarkerMap pose tracker
	MarkerMapPoseTracker mmTracker;

	// OpenCV matrices storing the images
	// current frame
	Mat currentFrameAruco;
	// Undistorted frame
	Mat undistortedFrame;
	// Resized image
	Mat resizedFrame;

	// Size of the OpenGL window size
	Size windowDim;

	// Methods
public:
	// Constructor
	ArucoGL(string intrinFileName, float markerSize, string mmFileName, float winWidth, float winHeight);
	// Destructor
	~ArucoGL();

	// Reading external geometry
	void objLoader();

	// Drawing function
	void drawBackground();
	void setWorldPosition();
	void drawGhostGeometry();
	void drawGeometry();

	/* Used in callbacks */
	void drawScene();
	void idle(Mat newFrame);

	/* Debug */
	void drawAxis (float size);

	// Draws a 3D view of the world with the markers and the camera with OpenGL
	void drawWorldModel();
};

#endif // GB_ARUCOGL
