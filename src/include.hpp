#pragma once

#ifndef __unix__
	#include <Windows.h>
#endif

#include <iostream>

// OpenGL/GLUT
#include <GL/gl.h>
#include <GL/freeglut.h>

// OpenCV
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Aruco
#include <aruco/aruco.h>

// Namespaces
using namespace std;
using namespace cv;
using namespace aruco;
