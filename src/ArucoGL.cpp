#include "ArucoGL.hpp"


// Constructor
ArucoGL::ArucoGL (string intrinFileName, float markerSize, string mmFileName, float winWidth, float winHeight) {
	// Initializing attributes
	intrinsicFile = intrinFileName;
	this->markerSize = markerSize;

	// read camera parameters and marker map config
	cameraParams.readFromXMLFile(intrinFileName);
	mmap.readFromFile(mmFileName);

	// config MarkerMap pose tracker
	mmTracker.setParams(cameraParams, mmap, this->markerSize);
	if ( ! mmTracker.isValid() ) {
		cerr << "Invalid MarkerMapTracker initialization" << endl;
	}

	windowDim = Size(winWidth, winHeight);
}

// Destructor
ArucoGL::~ArucoGL() {}

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cpp
#include <tiny_obj_loader.h>

void ArucoGL::objLoader() {

	std::string inputfile = "manoir_couleur.obj";
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string warn;
	std::string err;

	bool ret = tinyobj::LoadObj(
		&attrib, &shapes, &materials, &warn, &err, inputfile.c_str()
	);

	// Loop over shapes
	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;

		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];

			glBegin( GL_TRIANGLES );
			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				// access to vertex
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				tinyobj::real_t vx = attrib.vertices[3*idx.vertex_index+0];
				tinyobj::real_t vy = attrib.vertices[3*idx.vertex_index+1];
				tinyobj::real_t vz = attrib.vertices[3*idx.vertex_index+2];

				//cout << vx /10 << " " << vy /10 << " " << vz /10 << endl;
				// tinyobj::real_t nx = attrib.normals[3*idx.normal_index+0];
				// tinyobj::real_t ny = attrib.normals[3*idx.normal_index+1];
				// tinyobj::real_t nz = attrib.normals[3*idx.normal_index+2];
				// tinyobj::real_t tx = attrib.texcoords[2*idx.texcoord_index+0];
				// tinyobj::real_t ty = attrib.texcoords[2*idx.texcoord_index+1];
				// // Optional: vertex colors
				// tinyobj::real_t red = attrib.colors[3*idx.vertex_index+0];
				// tinyobj::real_t green = attrib.colors[3*idx.vertex_index+1];
				// tinyobj::real_t blue = attrib.colors[3*idx.vertex_index+2];

				glVertex3f(vx /100, vy / 100, vz / 100);
			}
			glEnd();
			index_offset += fv;

			// per-face material
			shapes[s].mesh.material_ids[f];
		}
	}

}

// Draw the camera frame in the background of the OpenGL window
void ArucoGL::drawBackground() {
	// If we do not have a frame we don't do anything
	if (resizedFrame.rows == 0)
		return;

	// Disabling depth test
	glDisable(GL_DEPTH_TEST);

	// Reset modelview matrix
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	// Define projection matrix & screen size
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho(0, windowDim.width, 0, windowDim.height, -1.0, 1.0);
	glViewport(0, 0, windowDim.width, windowDim.height);

	// Drawing pixels
	glRasterPos3f(0, windowDim.height, -1.0f);
	glPixelZoom(1, -1); // Drawing direction
	glDrawPixels(windowDim.width, windowDim.height, GL_RGB, GL_UNSIGNED_BYTE, resizedFrame.ptr(0));

	// Re-enabling depth test
	glEnable(GL_DEPTH_TEST);
}

// Get correct MVP matrixes relative to board from Aruco
void ArucoGL::setWorldPosition() {

	// Set the appropriate projection matrix so that rendering is done 
	// in an environment like the real camera (without distorsion)
	glMatrixMode(GL_PROJECTION);

	/* Load camera projection matrix */
	// FIXME ignore distortion parameters for now
	cameraParams.Distorsion = Scalar(0);

	double proj_matrix[16];
	cameraParams.glGetProjectionMatrix(
		resizedFrame.size(), windowDim, proj_matrix,
		/* near/far */ 0.01, 10
	);
	glLoadMatrixd(proj_matrix);


	/* Setting correct view matrix based on marker positions */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	// static so we keep the matrix even when we can't determine the camera pose
	static Mat RTMat;
	static float* modelview_matrix_p = nullptr;

	// Init status
	static bool mv_ok = false;

	bool foundPose = mmTracker.estimatePose(markersList);

	// Retrieve modelview matrix from the marker tracker
	if (foundPose) {
		RTMat = mmTracker.getRTMatrix();
		modelview_matrix_p = RTMat.ptr<float>( 0 ); // Pointer to the first data cell
		mv_ok = true;
	}

	if ( mv_ok ) {
		// Make the "projection matrix" look towards positive z
		static float invertZ[16] = {
			1, 0,  0, 0,
			0, 1,  0, 0,
			0, 0, -1, 0,
			0, 0,  0, 1
		};
		glLoadTransposeMatrixf( invertZ );

		glMultTransposeMatrixf( modelview_matrix_p );
	}
}

// Draw masking geometry
void ArucoGL::drawGhostGeometry() {

	// Disable writing in color buffer
	if (ghost) {
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	}
	// If color buffer is still activated, the geometry is displayed in white
	glColor3f(1.0f, 1.0f, 1.0f);
	
	glPushMatrix();

	// Draw geometry
	objLoader();

	glPopMatrix();

	// Re-enable writing in color buffer
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

}

// Draw visible geometry in scene
void ArucoGL::drawGeometry() {

	/* Draw ghost model */

	drawAxis(0.04);

	// Grey cube
	glColor3f( 0.7f, 0.7f, 0.7f );
	glutSolidCube(0.01);

	glPushMatrix();
	glTranslatef( 0.02f, 0.07f, 0.015f );
	glColor3f( 0.9f, 0.1f, 0.0f );
	glutSolidCube( 0.03f );
	glPopMatrix();

	/* Draw ghost */
	glPushMatrix();
	
	glTranslatef( 0.05f, 0.22f, 0.08f);
	glColor3f( 0.9f, 0.9f, 0.9f);

	glutSolidSphere( 0.02, 8, 8 );
	glutSolidCone( 0.02, -0.03, 8, 1 );

	glColor3f( 0.0f, 0.0f, 0.0f );

	glPushMatrix();
	glTranslatef( -0.015f, 0.01f, 0.01f);
	glutSolidSphere( 0.005, 8, 8 );
	glPopMatrix();

	glPushMatrix();
	glTranslatef( -0.015f, -0.01f, 0.01f);
	glutSolidSphere( 0.005, 8, 8 );
	glPopMatrix();

	glPopMatrix();


}

void ArucoGL::drawScene() {
	if (glScene) {
		// Display markers and camera positions
		//  as seen by OpenGL/Aruco
		drawWorldModel();
	
	} else {
		drawBackground();

		// Load projection and modelview matrices
		setWorldPosition();

		drawGhostGeometry();
		drawGeometry();
	}
}

void ArucoGL::idle(Mat newFrame) {
	// Getting new image
	currentFrameAruco = newFrame.clone();

	// transform color that by default is BGR to RGB
	// because windows systems do not allow reading BGR images with opengl properly
	cvtColor(currentFrameAruco, currentFrameAruco, COLOR_BGR2RGB);

	// TODO : undistort image ?

	// Detect markers in the frame
	markersList = mDetector.detect(currentFrameAruco);
	if (debugMode) {
		// Draw markers in the debug window
		for (auto marker : markersList) {
			marker.draw(currentFrame);
		}
	}

	// Resize to OpenGL window size if different
	resize(currentFrameAruco, resizedFrame, windowDim);
}

// Draw axis at modeling coordinates
void ArucoGL::drawAxis(float size) {
   // X
   glColor3f (1,0,0);
   glBegin(GL_LINES);
   glVertex3f(0.0f, 0.0f, 0.0f); // origin of the line
   glVertex3f(size,0.0f, 0.0f); // ending point of the line
   glEnd( );
   
   // Y
   glColor3f (0,1,0);
   glBegin(GL_LINES);
   glVertex3f(0.0f, 0.0f, 0.0f); // origin of the line
   glVertex3f(0.0f, size, 0.0f); // ending point of the line
   glEnd( );
   
   // Z
   glColor3f (0,0,1);
   glBegin(GL_LINES);
   glVertex3f(0.0f, 0.0f, 0.0f); // origin of the line
   glVertex3f(0.0f, 0.0f, size); // ending point of the line
   glEnd( );
}

void ArucoGL::drawWorldModel () {

	glEnable( GL_DEPTH_TEST );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective(
		5 /* deg fov */,
		static_cast<float>( windowDim.width ) / windowDim.height, // aspect ratio
		0.5, 10 // near/far
	);

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glTranslatef(0, 0, -2); // Move the objects into view

	// Rotation animation
	static float rotAngle = 0;
	glRotatef(rotAngle, 1, 0, 0); rotAngle ++;

	/* Draw origin */
	glColor3f(1.0f, 1.0f, 1.0f);
	glutWireCube( 0.01 );

	/* Draw axes */
	drawAxis(0.03);

	/* Draw markers */
	vector<int> markerIds;
	mmap.getIdList( markerIds, /* append */ false );

	for (int id : markerIds) {
		glColor3f( (static_cast<float>(id+1) / 3), 0.7f, 0.7f );
		auto m_info = mmap.getMarker3DInfo( id );

		// Draw marker
		glBegin( GL_QUADS );
		for (int i = 0; i < 4; i++) {
			// Each corner
			Point3f v = m_info[i];
			glVertex3f( v.x, v.y, v.z );
		}
		glEnd();

		// Draw it a second time (but opposite order) to have both sides
		glBegin( GL_QUADS );
		for (int i = 0; i < 4; i++) {
			Point3f v = m_info[4 - i -1];
			glVertex3f( v.x, v.y, v.z );
		}
		glEnd();
	}

	/* Draw camera as a green cube (position only) */
	bool foundPose = mmTracker.estimatePose(markersList);
	static bool tvec_init = false;

	if (foundPose || tvec_init) {
		static Mat Tvec = mmTracker.getTvec();
		tvec_init = true;
		cout << "Camera translation: " << Tvec << endl;

		glPushMatrix();
		glTranslatef( Tvec.at<float>(0), Tvec.at<float>(1), Tvec.at<float>(2) );
		glColor3f( 0.0f, 1.0f, 0.0f );
		glutSolidCube(0.01);
		glPopMatrix();
	}
}
