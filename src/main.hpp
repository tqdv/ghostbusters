#ifndef GB_MAIN
#define GB_MAIN

#include "include.hpp"

#include "ArucoGL.hpp"
#include "glut_callbacks.hpp"

// Defining escape key
#define KEY_ESCAPE 27

// GLOBAL VARIABLES TO USE WITH OPENGL ------------------------

// OpenCV components
int cameraID;
VideoCapture cap;

string ocv_debug; // Name for the OpenCV windows
bool debugMode; // Toggle for debug window display

Mat currentFrame; // Container for the current frame

int widthFrame; // Width of the image
int heightFrame; // Height of the image

// ArUco components
ArucoGL* arucoManager; // Custom class to use ArUco in OpenGL

// OpenGL components
GLint ogl_display; // OpenGL window object
bool ghost; // Toggle for ghost display
bool glScene = false; // Toggle for scene display

#endif //GB_MAIN